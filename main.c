/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vchornyi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/25 15:28:09 by vchornyi          #+#    #+#             */
/*   Updated: 2017/10/25 15:28:11 by vchornyi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <strings.h>
#include <ctype.h>

void print_results_str(char *actual, char *expected)
{
	if (actual == NULL && expected == NULL)
	{
		printf("ok ");
		return ;
	}
	else if (actual == NULL)
	{
		printf("KO (a: (null)| e: '%s') ", expected);
		return ;
	}
	else if (expected == NULL)
	{
		printf("KO (a: '%s'| e: (null))) ", actual);
		return ;
	}
	if (strcmp(actual, expected) == 0)
		printf("ok ");
	else
		printf("KO (a: '%s'| e: '%s') ", actual, expected);
}

void print_results_num(long long actual, long long expected)
{
	if (actual == expected)
		printf("ok ");
	else
		printf("KO (a: '%lld'| e: '%lld') ", actual, expected);
}

void	test_memset()
{
	size_t len = 5;
	int c = 'a';
	void *mem = malloc(sizeof(void) * len);
	void *mem2 = ft_memset(mem, c, len);

	void *mem1 = malloc(sizeof(void) * len);
	void *mem3 = memset(mem1, c, len);

	size_t len1 = 50;
	int c1 = '\0';
	void *mem4 = malloc(sizeof(void) * len1);
	ft_memset(mem4, c1, len1);

	void *mem5 = malloc(sizeof(void) * len1);
	memset(mem5, c1, len1);

	printf("ft_memset: ");
	print_results_str(mem, mem1);
	print_results_str(mem2, mem3);
	print_results_str(mem4, mem5);
	printf("\n");
}

void	test_bzero()
{
	size_t len = 5;
	void *mem = malloc(sizeof(void) * len);
	ft_bzero(mem, len);

	void *mem1 = malloc(sizeof(void) * len);
	bzero(mem1, len);

	size_t len1 = 50;
	void *mem4 = malloc(sizeof(void) * len1);
	ft_bzero(mem4, len1);

	void *mem5 = malloc(sizeof(void) * len1);
	bzero(mem5, len1);

	char str1[] = "test";
	char str2[] = "test";

	ft_bzero(str1, 5);
	bzero(str2, 5);

	printf("ft_bzero: ");
	print_results_str(mem, mem1);
	print_results_str(str1, str2);
	print_results_str(mem4, mem5);
	printf("\n");
}

void	test_memcpy()
{
	char str1[] = "BapBapBapBapBap";
	char str2[] = "olo ay ayolo ay ayolo ay ayolo ay ayolo ";

	char str01[] = "TastString";
	char str04[] = "TastString";
	char *str02 = malloc(7);
	str02 = &str01[4];
	str02[6] = '\0';
	char *str03 = malloc(7);
	str03 = &str04[4];
	str03[6] = '\0';
	char str05[] = "Destination";
	char str06[] = "Destination";

	char str10[] = "1234";
	char str11[] = "abcdef";
	char str12[] = "abcdef";

	char str7[] = "123";
	char str8[] = "    ";
	char str9[] = "    ";
	
	printf("ft_memcpy: ");
	print_results_str(ft_memcpy(str2, str1, 20), memcpy(str2, str1, 20));
	// print_results_str(ft_memcpy(str02, str01, 16), memcpy(str03, str04, 16));
	print_results_str(str02, str03);
	print_results_str(ft_memcpy(str05, str01, 10), memcpy(str06, str04, 10));
	print_results_str(str05, str06);
	print_results_str(str05, str06);
	print_results_str(ft_memcpy(str11, str10, 4), memcpy(str12, str10, 4));
	print_results_str(ft_memcpy(str8, str7, 4), memcpy(str9, str7, 4));
	printf("\n");
}

void	test_memccpy()
{
	char str11[] = "Source";
	char str12[] = "Destination";
	char str13[] = "Destination";

	char str1[] = "Source";
	char str2[] = "Destination";
	char str3[] = "Destination";

	printf("ft_memccpy: ");
	print_results_str(ft_memccpy(str12, str11, 'o', 4), memccpy(str13, str11, 'o', 4));
	print_results_str(str12,str13);	
	print_results_str(ft_memccpy(str2, str1, 'r', 5), memccpy(str3, str1, 'r', 5));
	print_results_str(str2,str3);
	printf("\n");
}

void	test_memmove()
{
	char str1[] = "TestString";
	char str4[] = "TestString";
	char *str2 = malloc(7);
	str2 = &str1[4];
	str2[6] = '\0';
	char *str3 = malloc(7);
	str3 = &str4[4];
	str3[6] = '\0';

	char str11[] = "TastString";
	char str14[] = "TastString";
	char *str12 = malloc(7);
	str12 = &str11[4];
	str12[6] = '\0';
	char *str13 = malloc(7);
	str13 = &str14[4];
	str13[6] = '\0';
	char str15[] = "Destination";
	char str16[] = "Destination";

	char	testA1[] = "abcdef";
	char	testA2[] = "abcdef";

	ft_memmove(testA1 + 1, testA1, 5);
	ft_memmove(testA1 + 1, testA1, 0);
	memmove(testA2 + 1, testA2, 5);
	memmove(testA2 + 1, testA2, 0);

	printf("ft_memmove: ");
	print_results_str(ft_memmove(str2, str1, 6),memmove(str3, str4, 6));
	print_results_str(str2,str3);
	// print_results_str(ft_memmove(str12, str11, 16),memmove(str13, str14, 16));
	print_results_str(str12,str13);
	print_results_str(ft_memmove(str15, str11, 10),memmove(str16, str14, 10));
	print_results_str(testA1, "aabcde");
	print_results_str(testA1, testA2);
	printf("\n");
}

void	test_memchr()
{
	char str1[] = "TastStringBap";

	printf("ft_memchr: ");
	print_results_str(ft_memchr(str1, 'T', 0),memchr(str1, 'T', 0));
	print_results_str(ft_memchr(str1, 'T', 2),memchr(str1, 'T', 2));
	print_results_str(ft_memchr(str1, 's', 2),memchr(str1, 's', 2));
	print_results_str(ft_memchr(str1, 's', 3),memchr(str1, 's', 3));
	printf("\n");
}

void	test_memcmp()
{
	char str1[] = "";
	char str2[] = "";

	char str11[] = "123";
	char str21[] = "123";

	char str111[] = "1234";
	char str211[] = "1235";

	printf("ft_memcmp: ");
	print_results_num(ft_memcmp(str1, str2, 4),memcmp(str1, str2, 4));
	print_results_num(ft_memcmp(str11, str21, 4),memcmp(str11, str21, 4));
	print_results_num(ft_memcmp(str111, str211, 3),memcmp(str111, str211, 3));
	print_results_num(ft_memcmp("aww", "bpp", 0), memcmp("aww", "bpp", 0));
	printf("\n");
}

void	test_strlen()
{
	char str1[] = "";
	char str11[] = "123";
	char str111[] = "1234";
	char str1111[] = "12345678901234567890";

	printf("ft_strlen: ");
	print_results_num(ft_strlen(str1),strlen(str1));
	print_results_num(ft_strlen(str11),strlen(str11));
	print_results_num(ft_strlen(str111),strlen(str111));
	print_results_num(ft_strlen(str1111),strlen(str1111));
	printf("\n");
}

void	test_strdup()
{
	printf("ft_strdup: ");
	print_results_str(ft_strdup("Test"), "Test");
	print_results_str(ft_strdup(""), "");
	print_results_str(ft_strdup("OLOLOLO"), "OLOLOLO");
	print_results_str(ft_strdup("OLO\0LOLO"), "OLO");
	printf("\n");
}

void	test_strcpy()
{
	char str1[] = "Source";
	char str2[] = "Destination";
	char str3[] = "Source";
	char str4[] = "Destination";

	char *dest1 = malloc(3);
	char *dest2 = malloc(3);
	dest1[2] = '\0';
	dest2[2] = '\0';

	printf("ft_strcpy: ");
	print_results_str(ft_strcpy(str2, str1), strcpy(str4, str3));
	print_results_str(ft_strcpy(dest1, "12"), strcpy(dest2, "12"));
	printf("\n");
}

void	test_strncpy()
{
	char str1[] = "Source";
	char str2[] = "Destination";
	char str3[] = "Source";
	char str4[] = "Destination";

	char *dest1 = malloc(3);
	char *dest2 = malloc(3);
	dest1[2] = '\0';
	dest2[2] = '\0';

	char str11[] = "Source";
	char str12[] = "Destintation";
	char str13[] = "Source";
	char str14[] = "Destination";

	printf("ft_strncpy: ");
	print_results_str(ft_strncpy(str2, str1, 4), strncpy(str4, str3, 4));
	print_results_str(ft_strncpy(dest1, "12", 1), strncpy(dest2, "12", 1));
	print_results_str(ft_strncpy(str12, str11, 10), strncpy(str14, str13, 10));
	printf("\n");
}

void	test_strcat()
{
	char *str1 = malloc(25);
	str1[0] = 'a';
	str1[1] = '\0';
	char *str3 = malloc(25);
	str3[0] = 'a';
	str3[1] = '\0';
	char str2[] = "AppendTestString";

	printf("ft_strcat: ");
	print_results_str(ft_strcat(str1, str2), strcat(str3, str2));
	print_results_str(str1, str3);
	printf("\n");
}

void	test_strncat()
{
	char *str1 = malloc(25);
	str1[0] = 'a';
	str1[1] = '\0';
	char *str3 = malloc(25);
	str3[0] = 'a';
	str3[1] = '\0';
	char str2[] = "AppendTestString";

	printf("ft_strncat: ");
	print_results_str(ft_strncat(str1, str2, 17), strncat(str3, str2, 17));
	print_results_str(str1, str3);
	printf("\n");
}

void	test_strlcat()
{
	char str1[30] = "ab";
	char str3[30] = "ab";
	char str2[] = "1234567890";

	char *str = "the cake is a lie !\0I'm hidden lol\r\n";
	char buff1[0xF00] = "there is no stars in the sky";
	char buff2[0xF00] = "there is no stars in the sky";
	size_t max = strlen("the cake is a lie !\0I'm hidden lol\r\n") + strlen("there is no stars in the sky");

	printf("ft_strlcat: ");
	print_results_num(ft_strlcat(str1, str2, 8), strlcat(str3, str2, 8));
	print_results_num(ft_strlcat(buff2, str, max), strlcat(buff1, str, max));
	print_results_str(buff2, buff1);
	print_results_str(str1, str3);
	printf("\n");
}

void	test_strchr()
{
	char *str1 = "StringStr";

	printf("ft_strchr: ");
	print_results_str(ft_strchr(str1, 'S'), strchr(str1, 'S'));
	print_results_str(ft_strchr(str1, 'z'), strchr(str1, 'z'));
	printf("\n");
}

void	test_strrchr()
{
	char *str1 = "StringStr";

	printf("ft_strrchr: ");
	print_results_str(ft_strrchr(str1, '\0'), strrchr(str1, '\0'));
	print_results_str(ft_strrchr(str1, 'i'), strrchr(str1, 'i'));
	printf("\n");
}

void	test_strstr()
{
	char *str = "abcdefgcde";
	char *str2 = "cde";

	printf("ft_strstr: ");
	print_results_str(ft_strstr(str, str2), strstr(str, str2));
	print_results_str(ft_strstr(str, "ab"), strstr(str, "ab"));
	print_results_str(ft_strstr(str, "ab1"), strstr(str, "ab1"));
	printf("\n");
}

void	test_strnstr()
{
	char *str = "abcdfgcd";
	char *str2 = "def";

	printf("ft_strnstr: ");
	print_results_str(ft_strnstr(str, str2, 9), strnstr(str, str2, 9));
	print_results_str(ft_strnstr(str, "df", 9), strnstr(str, "df", 9));
	print_results_str(ft_strnstr(str, "df", 4), strnstr(str, "df", 4));
	print_results_str(ft_strnstr(str, "gcd", 8), strnstr(str, "gcd", 8));
	printf("\n");
}

void	test_strcmp()
{
	char str1[] = "Test";
	char str2[] = "Test1";

	printf("ft_strcmp: ");
	print_results_num(ft_strcmp(str1, str2), strcmp(str1, str2));
	print_results_num(ft_strcmp("str1", "str2"), strcmp("str1", "str2"));
	print_results_num(ft_strcmp("str11", "str2"), strcmp("str11", "str2"));
	print_results_num(ft_strcmp("", ""), strcmp("", ""));
	print_results_num(ft_strcmp("", "1"), strcmp("", "1"));
	printf("\n");
}

void	test_strncmp()
{
	char str1[] = "Test1\200ab";
	char str2[] = "Test1\200abc";

	printf("ft_strncmp: ");
	print_results_num(ft_strncmp(str1, str2, 9), strncmp(str1, str2, 9));
	print_results_num(ft_strncmp(str1, str2, 7), strncmp(str1, str2, 7));
	print_results_num(ft_strncmp("str1", "str2", 2), strncmp("str1", "str2", 2));
	print_results_num(ft_strncmp("str11", "str2", 3), strncmp("str11", "str2", 3));
	print_results_num(ft_strncmp("", "", 0), strncmp("", "", 0));
	print_results_num(ft_strncmp("", "1", 1), strncmp("", "1", 1));
	printf("\n");
}

void	test_atoi()
{

	char str1[] = "  --2";
	char str2[] = "  -2147483648";
	char str3[] = "  0002147483647";
	char str4[] = "  +123124asfasf142";
	char str5[] = " asf -2asfasf";
	char str6[] = "234a123";
	char str7[] = "atoi";
	char str8[] = "9223372036854775812";
	char str9[] = "-9223372036854775812";
	char str10[] = "9223372036854775807";
	char str11[] = "-9223372036854775807";

	printf("ft_atoi: ");
	print_results_num(ft_atoi(str1), atoi(str1));
	print_results_num(ft_atoi(str2), atoi(str2));
	print_results_num(ft_atoi(str3), atoi(str3));
	print_results_num(ft_atoi(str4), atoi(str4));
	print_results_num(ft_atoi(str5), atoi(str5));
	print_results_num(ft_atoi(str6), atoi(str6));
	print_results_num(ft_atoi(str7), atoi(str7));
	print_results_num(ft_atoi(str8), atoi(str8));
	print_results_num(ft_atoi(str9), atoi(str9));
	print_results_num(ft_atoi(str10), atoi(str10));
	print_results_num(ft_atoi(str11), atoi(str11));
	printf("\n");
}

void	test_isalpha()
{
	printf("ft_isalpha: ");
	print_results_num(ft_isalpha('\171'), isalpha('\171'));
	print_results_num(ft_isalpha('a'), isalpha('a'));
	print_results_num(ft_isalpha('z'), isalpha('z'));
	print_results_num(ft_isalpha('A'), isalpha('A'));
	print_results_num(ft_isalpha('Z'), isalpha('Z'));
	print_results_num(ft_isalpha('1'), isalpha('1'));
	printf("\n");
}

void	test_isdigit()
{
	printf("ft_isdigit: ");
	print_results_num(ft_isdigit('\171'), isdigit('\171'));
	print_results_num(ft_isdigit('a'), isdigit('a'));
	print_results_num(ft_isdigit('z'), isdigit('z'));
	print_results_num(ft_isdigit('A'), isdigit('A'));
	print_results_num(ft_isdigit('Z'), isdigit('Z'));
	print_results_num(ft_isdigit('1'), isdigit('1'));
	print_results_num(ft_isdigit('9'), isdigit('9'));
	printf("\n");
}

void	test_isalnum()
{
	printf("ft_isalnum: ");
	print_results_num(ft_isalnum('\171'), isalnum('\171'));
	print_results_num(ft_isalnum('a'), isalnum('a'));
	print_results_num(ft_isalnum('z'), isalnum('z'));
	print_results_num(ft_isalnum('A'), isalnum('A'));
	print_results_num(ft_isalnum('Z'), isalnum('Z'));
	print_results_num(ft_isalnum('1'), isalnum('1'));
	print_results_num(ft_isalnum('9'), isalnum('9'));
	printf("\n");
}

void	test_isascii()
{
	printf("ft_isascii: ");
	print_results_num(ft_isascii(0x7f), isascii(0x7f));
	print_results_num(ft_isascii('a'), isascii('a'));
	print_results_num(ft_isascii(012), isascii(012));
	print_results_num(ft_isascii('A'), isascii('A'));
	print_results_num(ft_isascii('Z'), isascii('Z'));
	print_results_num(ft_isascii('1'), isascii('1'));
	print_results_num(ft_isascii(127), isascii(127));
	print_results_num(ft_isascii(128), isascii(128));
	printf("\n");
}

void	test_isprint()
{
	printf("ft_isprint: ");
	print_results_num(ft_isprint(0x7f), isprint(0x7f));
	print_results_num(ft_isprint('a'), isprint('a'));
	print_results_num(ft_isprint(012), isprint(012));
	print_results_num(ft_isprint('A'), isprint('A'));
	print_results_num(ft_isprint(040), isprint(040));
	print_results_num(ft_isprint('1'), isprint('1'));
	print_results_num(ft_isprint(127), isprint(127));
	print_results_num(ft_isprint(128), isprint(128));
	printf("\n");
}

void	test_toupper()
{
	char test1[] = "Test str";
	char test2[] = "Test str";
	int i = 0;
	while (i < 9)
	{
		test1[i] = ft_toupper(test1[i]);
		test2[i] = toupper(test2[i]);
		i++;
	}
	printf("ft_toupper: ");
	print_results_str(test1, test2);
	printf("\n");
}

void	test_tolower()
{
	char test1[] = "Test Str";
	char test2[] = "Test Str";
	int i = 0;
	while (i < 9)
	{
		test1[i] = ft_tolower(test1[i]);
		test2[i] = tolower(test2[i]);
		i++;
	}
	printf("ft_tolower: ");
	print_results_str(test1, test2);
	printf("\n");
}

void	test_memalloc()
{
	char *str = (char *)ft_memalloc(10);

	printf("ft_memalloc: ");
	print_results_str(str, "");
	printf("\n");
}

void	test_memdel()
{
	char *str = (char *)malloc(sizeof(char) * 2);
	str[0] = '1';
	str[1] = '\0';
	char *str1 = NULL;
	ft_memdel((void *)&str);
	ft_memdel((void *)&str1);
	ft_memdel(NULL);

	printf("ft_memdel: ");
	print_results_str(str, NULL);
	print_results_str(str1, NULL);
	printf("\n");
}

void	test_strnew()
{
	char *str = ft_strnew(1);
	char *str1 = ft_strnew(1);
	str1[0] = '9';
	char *str2 = ft_strnew(1);
	str2[0] = '9';
	str[0] = '1';
	str[1] = '2';
	str[2] = '3';
	str[3] = '5';

	printf("ft_strnew: ");
	print_results_str(str, "1235");
	printf("\n");
}

void	test_strdel()
{
	char *str = malloc(3);
	str[0] = '1';
	str[1] = '2';
	str[2] = '\0';
	ft_strdel(&str);

	printf("ft_strdel: ");
	print_results_str(str, NULL);
	printf("\n");
}

void	test_strclr()
{
	char str[50] = "Test";
	char str1[50] = "str";
	char str2[50] = "Test Test Test";
	char str3[50] = "ALALALALALAL OLOLOLOL";
	ft_strclr(str);
	ft_strclr(str1);
	ft_strclr(str2);
	ft_strclr(str3);
	ft_strclr(NULL);

	printf("ft_strclr: ");
	print_results_str(str, "");
	print_results_str(str1, "");
	print_results_str(str2, "");
	print_results_str(str3, "");
	printf("\n");
}

char g_striter[5];
int g_striter_i = 0;

void f_striter(char *s)
{
	g_striter[g_striter_i] = s[0];
	g_striter_i++;
}

void			uf_striter_callback(char *s)
{
	*s = *s + 1;
}

void	test_striter()
{
	char str[5] = "Test";
	ft_striter(str, &f_striter);

	ft_striter(NULL, &f_striter);

	char		str42[] = "Hello";
	ft_striter(str42, uf_striter_callback);

	printf("ft_striter: ");
	print_results_str(str, g_striter);
	print_results_str(str42, "Ifmmp");
	printf("\n");
}

char g_striteri[5];

void			uf_striteri_callback(unsigned int i, char *s)
{
	*s = *s + i;
}

void f_striteri(unsigned int i, char *s)
{
	g_striter[i] = s[0];
}

void	test_striteri()
{
	char str[5] = "Te0st";
	ft_striteri(str, &f_striteri);
	ft_striteri(NULL, &f_striteri);

	char		str42[] = "Hello";
	ft_striteri(str42, uf_striteri_callback);

	printf("ft_striteri: ");
	print_results_str(str, g_striter);
	print_results_str(str42, "Hfnos");
	printf("\n");
}

char f_strmap(char s)
{
	return ('C');
}

void	test_strmap()
{
	char str[] = "Te0st";
	char *str1 = ft_strmap(str, &f_strmap);

	printf("ft_strmap: ");
	print_results_str(str1, "CCCCC");
	printf("\n");
}

char f_strmapi(unsigned int i, char s)
{
	return (i + '0');
}

void	test_strmapi()
{
	char str[] = "Te0st";
	char *str1 = ft_strmapi(str, &f_strmapi);

	printf("ft_strmapi: ");
	print_results_str(str1, "01234");
	printf("\n");
}

void	test_strequ()
{
	printf("ft_strequ: ");
	print_results_num(ft_strequ("Test", "Test"), 1);
	print_results_num(ft_strequ("Test", "Test1"), 0);
	print_results_num(ft_strequ("Test", "Tes"), 0);
	printf("\n");
}

void	test_strnequ()
{
	printf("ft_strnequ: ");
	print_results_num(ft_strnequ("Test", "Test", 4), 1);
	print_results_num(ft_strnequ("Test", "Test1", 4), 1);
	print_results_num(ft_strnequ("Test", "Test1", 5), 0);
	printf("\n");
}

void	test_strsub()
{
	printf("ft_strsub: ");
	print_results_str(ft_strsub("TestString", 1, 4), "estS");
	print_results_str(ft_strsub("Test", 0, 4), "Test");
	print_results_str(ft_strsub("0123456", 0, 7), "0123456");
	print_results_str(ft_strsub("012345689", 0, 6), "012345");
	printf("\n");
}

void	test_strjoin()
{
	printf("ft_strjoin: ");
	print_results_str(ft_strjoin("Test", " String "), "Test String ");
	print_results_str(ft_strjoin("Test", "1"), "Test1");
	print_results_str(ft_strjoin("0123456", "789 "), "0123456789 ");
	printf("\n");
}

void	test_strtrim()
{
	printf("ft_strtrim: ");
	print_results_str(ft_strtrim("\t \nTest\t\n  "), "Test");
	print_results_str(ft_strtrim("   Test  "), "Test");
	print_results_str(ft_strtrim(" 012   3456 "), "012   3456");
	print_results_str(ft_strtrim(""), "");
	printf("\n");
}

void	test_strsplit()
{
	char **test1 = ft_strsplit("****ONE***hello*", '*');
	char **test2 = ft_strsplit("aaa\0bbb\0", '\0'); 
    char **test3 = ft_strsplit("      split       this for   me  !       ", ' ');
    char **tt = ft_strsplit("***salut****!**", '*');

	printf("ft_strsplit: ");
	print_results_str(test1[0], "ONE");
	print_results_str(test1[1], "hello");
	print_results_str(test1[2], NULL);
	print_results_str(test2[0], "aaa");
	print_results_str(test2[1], NULL);
	print_results_str(test3[0], "split");
	print_results_str(test3[4], "!");
	print_results_str(tt[0], "salut");
	print_results_str(tt[1], "!");
	print_results_str(tt[2], NULL);
	printf("\n");
}

void	test_itoa()
{
	printf("ft_itoa: ");
	print_results_str(ft_itoa(-1), "-1");
	print_results_str(ft_itoa(0), "0");
	print_results_str(ft_itoa(1), "1");
	print_results_str(ft_itoa(1234567890), "1234567890");
	print_results_str(ft_itoa(2147483647), "2147483647");
	print_results_str(ft_itoa(-2147483648), "-2147483648");
	printf("\n");
}

void	test_putchar_str()
{
	ft_putstr("ft_putchar: ");
	ft_putchar('o');
	ft_putchar('k');	
	ft_putchar('\n');

	ft_putstr("ft_putstr: ");
	ft_putstr("ok\n");

	ft_putstr("ft_putendl: ");
	ft_putendl("ok");
}

void	test_putnbr()
{
	ft_putstr("ft_putnbr: ");
	ft_putnbr(-1);
	ft_putstr("==-1  ");
	ft_putnbr(0);
	ft_putstr("==0  ");
	ft_putnbr(1);
	ft_putstr("==1  ");
	ft_putnbr(2147483647);
	ft_putstr("==2147483647  ");
	ft_putnbr(-2147483648);
	ft_putstr("==-2147483648");
	ft_putstr("\n");
}

void	test_putchar_str_fd()
{
	ft_putstr_fd("ft_putchar_fd: ", 1);
	ft_putchar_fd('o', 1);
	ft_putchar_fd('k', 1);	
	ft_putchar_fd('\n', 1);

	ft_putstr_fd("ft_putstr_fd: ", 1);
	ft_putstr_fd("ok\n", 1);

	ft_putstr_fd("ft_putendl_fd: ", 1);
	ft_putendl_fd("ok", 1);
}

void	test_putnbr_fd()
{
	ft_putstr_fd("ft_putnbr_fd: ", 1);
	ft_putnbr_fd(-1, 1);
	ft_putstr_fd("==-1  ", 1);
	ft_putnbr_fd(0, 1);
	ft_putstr_fd("==0  ", 1);
	ft_putnbr_fd(1, 1);
	ft_putstr_fd("==1  ", 1);
	ft_putnbr_fd(2147483647, 1);
	ft_putstr_fd("==2147483647  ", 1);
	ft_putnbr_fd(-2147483648, 1);
	ft_putstr_fd("==-2147483648", 1);
	ft_putstr_fd("\n", 1);
}

void	test_ft_lstnew()
{
	char str[] = "Test";
	t_list *test = ft_lstnew(str, sizeof(str));

	printf("ft_lstnew: ");
	print_results_str(test->content, str);
	print_results_num(test->content_size, 5);
	printf("\n");
}

void f_ft_lstdelone(void *str, size_t size)
{
	char *str1 = (char *)str;
	if (size > 0)
		str1[0] = '0';
}

void	test_ft_lstdelone()
{
	char str1[] = "Test1";
	t_list *list1 = ft_lstnew(str1, sizeof(str1));
	char str2[] = "Test2";
	t_list *list2 = ft_lstnew(str2, sizeof(str2));
	char str3[] = "Test3";
	t_list *list3 = ft_lstnew(str3, sizeof(str3));
	char str4[] = "Test4";
	t_list *list4 = ft_lstnew(str4, sizeof(str4));

	list1->next = list2;
	list2->next = list3;
	list3->next = list4;

	ft_lstdelone(&list2, &f_ft_lstdelone);

	printf("ft_lstdelone: ");
	print_results_str(list1->content, str1);
	if (list2 == NULL)
		print_results_str("null", "null");
	else
		print_results_str("ok", "null");
	print_results_str(list3->content, str3);
	print_results_str(list4->content, str4);
	printf("\n");
}


void	test_ft_lstdel()
{
	char str1[] = "Test1";
	t_list *list1 = ft_lstnew(str1, sizeof(str1));
	char str2[] = "Test2";
	t_list *list2 = ft_lstnew(str2, sizeof(str2));
	char str3[] = "Test3";
	t_list *list3 = ft_lstnew(str3, sizeof(str3));
	char str4[] = "Test4";
	t_list *list4 = ft_lstnew(str4, sizeof(str4));

	list1->next = list2;
	list2->next = list3;
	list3->next = list4;

	ft_lstdel(&list2, &f_ft_lstdelone);

	printf("ft_lstdel: ");
	print_results_str(list1->content, str1);
	if (list2 == NULL)
		print_results_str("null", "null");
	else
		print_results_str("ok", "null");
	printf("\n");
}

void	test_ft_lstadd()
{
	char str1[] = "Test1";
	t_list *list1 = ft_lstnew(str1, sizeof(str1));
	char str2[] = "Test2";
	t_list *list2 = ft_lstnew(str2, sizeof(str2));
	char str3[] = "Test3";
	t_list *list3 = ft_lstnew(str3, sizeof(str3));
	char str4[] = "Test4";
	t_list *list4 = ft_lstnew(str4, sizeof(str4));

	ft_lstadd(&list4, list3);
	ft_lstadd(&list4, list2);
	ft_lstadd(&list4, list1);

	printf("ft_lstadd: ");
	print_results_str(list4->next->content, str2);
	print_results_str(list4->next->next->content, str3);
	print_results_str(list4->next->next->next->content, str4);
	printf("\n");
}

void f_lst_iter(t_list *elem)
{
	printf("%s", elem->content);
}

void	test_ft_lstiter()
{
	char str1[] = " ok";
	t_list *list1 = ft_lstnew(str1, sizeof(str1));
	char str2[] = " ok";
	t_list *list2 = ft_lstnew(str2, sizeof(str2));
	char str3[] = " ok";
	t_list *list3 = ft_lstnew(str3, sizeof(str3));
	char str4[] = " ok";
	t_list *list4 = ft_lstnew(str4, sizeof(str4));

	list1->next = list2;
	list2->next = list3;
	list3->next = list4;

	printf("ft_lstiter:");
	ft_lstiter(list1, &f_lst_iter);
	printf("\n");
}

t_list * f_re_create(t_list *elem)
{
	((char *)elem->content)[1] = 'o';
	elem->content_size = 100;
	return(elem);
}

void f_ft_lstmap(t_list *elem)
{
	printf("%s", elem->content);
}

void	test_ft_lstmap()
{
	char str1[] = " kk";
	t_list *list1 = ft_lstnew(str1, sizeof(str1));
	char str2[] = " kk";
	t_list *list2 = ft_lstnew(str2, sizeof(str2));
	char str3[] = " kk";
	t_list *list3 = ft_lstnew(str3, sizeof(str3));
	char str4[] = " kk";
	t_list *list4 = ft_lstnew(str4, sizeof(str4));

	list1->next = list2;
	list2->next = list3;
	list3->next = list4;

	t_list *new = ft_lstmap(list1, &f_re_create);

	printf("ft_lstmap:");
	ft_lstiter(new, &f_ft_lstmap);
	printf("\n");
}

// void	test_ft_lstaddend()
// {
// 	char str1[] = "Test1";
// 	t_list *list1 = ft_lstnew(str1, sizeof(str1));
// 	char str2[] = "Test2";
// 	t_list *list2 = ft_lstnew(str2, sizeof(str2));
// 	char str3[] = "Test3";
// 	t_list *list3 = ft_lstnew(str3, sizeof(str3));
// 	char str4[] = "Test4";
// 	t_list *list4 = ft_lstnew(str4, sizeof(str4));

// 	t_list *null = NULL;

// 	ft_lstaddend(&list1, list2);
// 	ft_lstaddend(&list1, list3);
// 	ft_lstaddend(&list1, list4);
// 	ft_lstaddend(&list1, null);

// 	printf("ft_lstaddend: ");
// 	print_results_str(list1->content, str1);
// 	print_results_str(list1->next->content, str2);
// 	print_results_str(list1->next->next->content, str3);
// 	print_results_str(list1->next->next->next->content, str4);
// 	printf("\n");
// }

// void	test_ft_lstat()
// {
// 	char str1[] = "Test1";
// 	t_list *list1 = ft_lstnew(str1, sizeof(str1));
// 	char str2[] = "Test2";
// 	t_list *list2 = ft_lstnew(str2, sizeof(str2));
// 	char str3[] = "Test3";
// 	t_list *list3 = ft_lstnew(str3, sizeof(str3));
// 	char str4[] = "Test4";
// 	t_list *list4 = ft_lstnew(str4, sizeof(str4));

// 	t_list *null = NULL;

// 	ft_lstaddend(&list1, list2);
// 	ft_lstaddend(&list1, list3);
// 	ft_lstaddend(&list1, list4);
// 	ft_lstaddend(&list1, null);

// 	printf("ft_lstat: ");
// 	print_results_str(ft_lstat(list1, 1)->content, str1);
// 	print_results_str(ft_lstat(list1, 2)->content, str2);
// 	print_results_str(ft_lstat(list1, 3)->content, str3);
// 	print_results_str(ft_lstat(list1, 4)->content, str4);
// 	printf("\n");
// }

// void	test_ft_lstsize()
// {
// 	char str1[] = "Test1";
// 	t_list *list1 = ft_lstnew(str1, sizeof(str1));
// 	char str2[] = "Test2";
// 	t_list *list2 = ft_lstnew(str2, sizeof(str2));
// 	char str3[] = "Test3";
// 	t_list *list3 = ft_lstnew(str3, sizeof(str3));
// 	char str4[] = "Test4";
// 	t_list *list4 = ft_lstnew(str4, sizeof(str4));

// 	t_list *null = NULL;
// 	ft_lstaddend(&list1, list2);

// 	printf("ft_lstsize: ");
// 	print_results_num(ft_lstsize(list1), 2);
// 	ft_lstaddend(&list1, list3);
// 	ft_lstaddend(&list1, list4);
// 	ft_lstaddend(&list1, null);
// 	print_results_num(ft_lstsize(list1), 4);
// 	printf("\n");
// }

// char *read_file(char *file_name)
// {
// 	int		file_desc;
// 	char	*res;

// 	file_desc = open(file_name, O_RDONLY);
// 	if (file_desc == -1)
// 		return (NULL);
// 	res = ft_readfile(file_desc, 2);
// 	if (close(file_desc) == -1)
// 		return (NULL);
// 	return (res);
// }

// void	test_ft_readfile()
// {
// 	char *str1 = read_file("author");

// 	printf("ft_readfile: ");
// 	print_results_str(str1, "vchornyi\n");
// 	printf("\n");
// }

// void	test_ft_strdupaddmem()
// {
// 	size_t buf = 10;
// 	char *str = ft_strnew(buf);
// 	int i = 0;
// 	while (i < 10)
// 	{
// 		str[i] = i + '0';
// 		i++;
// 	}
// 	char *res = ft_strdupaddmem(str, &buf);

// 	printf("ft_strdupaddmem: ");
// 	print_results_str(res, "0123456789");
// 	print_results_num(buf, 15);
// 	printf("\n");
// }

// void	test_ft_strrev()
// {
// 	printf("ft_strrev: ");
// 	print_results_str(ft_strrev(NULL), NULL);
// 	print_results_str(ft_strrev("null"), "llun");
// 	print_results_str(ft_strrev("Test"), "tseT");
// 	printf("\n");
// }

int	main()
{
	printf("    ----     FIRST PART    ----     \n");
	test_memset();
	test_bzero();
	test_memcpy();
	test_memccpy();
	test_memmove();
	test_memchr();
	test_memcmp();
	test_strlen();
	test_strdup();
	test_strcpy();
	test_strncpy();
	test_strcat();
	test_strncat();
	test_strlcat();
	test_strchr();
	test_strrchr();
	test_strstr();
	test_strnstr();
	test_strcmp();
	test_strncmp();
	test_atoi();
	test_isalpha();
	test_isdigit();
	test_isalnum();
	test_isascii();
	test_isprint();
	test_toupper();
	test_tolower();
	printf("    ----     SECOND PART    ----     \n");
	test_memalloc();
	test_memdel();
	test_strnew();
	test_strdel();
	test_strclr();
	test_striter();
	test_striteri();
	test_strmap();
	test_strmapi();
	test_strequ();
	test_strnequ();
	test_strsub();
	test_strjoin();
	test_strtrim();
	test_strsplit();
	test_itoa();
	test_putchar_str();
	test_putnbr();
	test_putchar_str_fd();
	test_putnbr_fd();
	printf("    ----     BONUS PART    ----     \n");
	test_ft_lstnew();
	test_ft_lstdelone();
	test_ft_lstdel();
	test_ft_lstadd();
	test_ft_lstiter();
	test_ft_lstmap();


	// printf("    ----     ADDITIONAL FUNCTIONS    ----     \n");
	// test_ft_lstaddend();
	// test_ft_lstat();
	// test_ft_lstsize();
	// test_ft_readfile();
	// test_ft_strdupaddmem();
	// test_ft_strrev();
	return 0;
}
